# import math
# import maya.cmds as mc
#
# if len(mc.ls(type='locator')) > 0:
#     mc.delete(mc.ls(type='locator'))
#
# a = 0.2
# b = 0.2
#
# for i in xrange(0, 360 * 4, 45):
#     theta = math.radians(i)
#     r = a * math.exp(b * theta)
#
#     x = r * math.cos(theta)
#     y = 0
#     z = r * math.sin(theta)
#
#     mc.spaceLocator(position=[x, y, z])

import math

import maya.api.OpenMaya as OpenMaya



kCommandName            = 'spLogSpiralCurve'



################################################################################
# @brief      Tells maya to use the new python API
#
def maya_useNewAPI():
    pass


################################################################################
# @brief      Class for sp spiral curve.
#
class spSpiralCurve(OpenMaya.MPxCommand):

    kAConstantFlag      = '-a'
    kAConstantLongFlag  = '-aConstant'

    kBConstantFlag      = '-b'
    kBConstantLongFlag  = '-bConstant'

    radius              = 1.0
    aConstant           = 1.0
    bConstant           = 1.0

    dgMod               = None

    ############################################################################
    # @brief      Constructs the object.
    #
    def __init__(self):
        super(spSpiralCurve, self).__init__()

    ############################################################################
    # @brief      Creates the spSpiralCurve command
    # @return     The spSpiralCurve instance
    #
    @staticmethod
    def creator():
        return spSpiralCurve()

    ############################################################################
    # @brief      Sets the syntax.
    # @return     The new created syntax
    #
    @staticmethod
    def setSyntax():
        syntax = OpenMaya.MSyntax()
        syntax.addFlag(spSpiralCurve.kAConstantFlag, spSpiralCurve.kAConstantLongFlag, OpenMaya.MSyntax.kDouble)
        syntax.addFlag(spSpiralCurve.kBConstantFlag, spSpiralCurve.kBConstantLongFlag, OpenMaya.MSyntax.kDouble)

        return syntax

    ############################################################################
    # @brief      Parses the arguments and defines the local variable for the class
    #
    # @param      self  The object
    # @param      args  The arguments
    #
    def parseArguments(self, args):
        argDB = OpenMaya.MArgDatabase(self.syntax(), args)

        if argDB.isFlagSet(self.kAConstantFlag):
            self.aConstant = argDB.flagArgumentDouble(self.kAConstantFlag, 0)

        if argDB.isFlagSet(self.kBConstantFlag):
            self.bConstant = argDB.flagArgumentDouble(self.kBConstantFlag, 0)

    ############################################################################
    # @brief      Executes the actual command
    #
    # @param      self  The object
    # @param      args  The arguments
    #
    def doIt(self, args):
        self.parseArguments(args)

        # self.dgMod = OpenMaya.MDGModifier()

        curve = OpenMaya.MFnNurbsCurve()
        curvePoints = OpenMaya.MPointArray()
        curveKnots = OpenMaya.MDoubleArray()

        deg         = 3
        ncvs        = 20
        spans       = ncvs - deg
        nknots      = spans + 2 * deg - 1

        rotations = 4

        degAngle = 360 * rotations
        step = degAngle / ncvs

        for i in xrange(0, degAngle, step):
            theta = math.radians(i)
            r = self.aConstant * math.exp(self.bConstant * theta)

            x = r * math.cos(theta)
            y = 0
            z = r * math.sin(theta)

            curvePoints.append(OpenMaya.MPoint(x, y, z))

        for i in xrange(0, nknots):
            curveKnots.append(i)

        curve.create(curvePoints, curveKnots, deg, OpenMaya.MFnNurbsCurve.kOpen, 0, 0)

        self.setResult(curve.fullPathName())

    ############################################################################
    # @brief      Redo the current command
    # @param      self  The object
    #
    def redoIt(self):
        pass

    ############################################################################
    # @brief      Undo the current command.
    # @param      self  The object
    #
    def undoIt(self):
        pass

    ############################################################################
    # @brief      Determines if undoable.
    # @param      self  The object
    # @return     True if undoable, False otherwise.
    #
    def isUndoable(self):
        return True


################################################################################
# @brief      Inizialize the plugin
# @param      mobject  The mobject
#
def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "Any")
    mplugin.registerCommand(kCommandName, spSpiralCurve.creator, spSpiralCurve.setSyntax)

################################################################################
# @brief      Uninitialize the plugin
# @param      mobject  The mobject
#
def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)
    mplugin.deregisterCommand(kCommandName)

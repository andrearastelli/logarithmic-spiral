import sys
import math

import maya.api.OpenMaya as OpenMaya


kNodeName = 'logSpiralCurve'


def maya_useNewAPI():
    pass


class spLogSpiralCurve(OpenMaya.MPxNode):

    id = OpenMaya.MTypeId(0x001054F0)

    inNurbsCurve = None
    inAConstant = None
    inBConstant = None
    inSpires = None
    inNumCV = None

    outNurbsCurve = None

    def __init__(self):
        super(spLogSpiralCurve, self).__init__()

    @staticmethod
    def creator():
        return spLogSpiralCurve()

    @staticmethod
    def initialize():
        inNurbsCurveFn = OpenMaya.MFnTypedAttribute()
        spLogSpiralCurve.inNurbsCurve = inNurbsCurveFn.create('inNurbsCurve', 'ic', OpenMaya.MFnNurbsCurveData.kNurbsCurve)
        inNurbsCurveFn.storable = True
        inNurbsCurveFn.keyable = False
        inNurbsCurveFn.readable = True
        inNurbsCurveFn.writable = True
        inNurbsCurveFn.cached = False
        OpenMaya.MPxNode.addAttribute(spLogSpiralCurve.inNurbsCurve)

        inAConstantFn = OpenMaya.MFnNumericAttribute()
        spLogSpiralCurve.inAConstant = inAConstantFn.create('inAConstant', 'ac', OpenMaya.MFnNumericData.kDouble, 0.1)
        inAConstantFn.storable = True
        inAConstantFn.keyable = True
        inAConstantFn.readable = True
        inAConstantFn.writable = True
        OpenMaya.MPxNode.addAttribute(spLogSpiralCurve.inAConstant)

        inBConstantFn = OpenMaya.MFnNumericAttribute()
        spLogSpiralCurve.inBConstant = inBConstantFn.create('inBConstant', 'bc', OpenMaya.MFnNumericData.kDouble, 0.1)
        inBConstantFn.storable = True
        inBConstantFn.keyable = True
        inBConstantFn.readable = True
        inBConstantFn.writable = True
        OpenMaya.MPxNode.addAttribute(spLogSpiralCurve.inBConstant)

        inSpiresFn = OpenMaya.MFnNumericAttribute()
        spLogSpiralCurve.inSpires = inSpiresFn.create('inSpires', 's', OpenMaya.MFnNumericData.kDouble, 1)
        inSpiresFn.storable = True
        inSpiresFn.keyable = True
        inSpiresFn.readable = True
        inSpiresFn.writable = True
        OpenMaya.MPxNode.addAttribute(spLogSpiralCurve.inSpires)

        inNumCVFn = OpenMaya.MFnNumericAttribute()
        spLogSpiralCurve.inNumCV = inNumCVFn.create('numCVs', 'ncv', OpenMaya.MFnNumericData.kInt, 0)

        outNurbsCurveFn = OpenMaya.MFnTypedAttribute()
        spLogSpiralCurve.outNurbsCurve = outNurbsCurveFn.create('outNurbsCurve', 'oc', OpenMaya.MFnNurbsCurveData.kNurbsCurve)
        outNurbsCurveFn.storable = False
        outNurbsCurveFn.keyable = False
        outNurbsCurveFn.readable = True
        outNurbsCurveFn.writable = False
        OpenMaya.MPxNode.addAttribute(spLogSpiralCurve.outNurbsCurve)

        OpenMaya.MPxNode.attributeAffects(spLogSpiralCurve.inNurbsCurve, spLogSpiralCurve.outNurbsCurve)
        OpenMaya.MPxNode.attributeAffects(spLogSpiralCurve.inAConstant, spLogSpiralCurve.outNurbsCurve)
        OpenMaya.MPxNode.attributeAffects(spLogSpiralCurve.inBConstant, spLogSpiralCurve.outNurbsCurve)
        OpenMaya.MPxNode.attributeAffects(spLogSpiralCurve.inSpires, spLogSpiralCurve.outNurbsCurve)

    def spiralCoordinate(self, theta, a, b):
        r = a * math.exp(theta * b)
        x = r * math.cos(theta)
        y = 0
        z = r * math.sin(theta)

        return OpenMaya.MPoint(x, y, z)

    def compute(self, plug, data):
        assert(isinstance(data.context(), OpenMaya.MDGContext))
        assert(data.setContext(data.context()) == data)

        if plug != spLogSpiralCurve.outNurbsCurve:
            return None
        
        inNurbsCurveDataHandle = data.inputValue(spLogSpiralCurve.inNurbsCurve)
        inCurve = inNurbsCurveDataHandle.asNurbsCurveTransformed()
        curve = OpenMaya.MFnNurbsCurve(inCurve)

        inAConstantDataHandle = data.inputValue(spLogSpiralCurve.inAConstant)
        inA = inAConstantDataHandle.asDouble()

        inBConstantDataHandle = data.inputValue(spLogSpiralCurve.inBConstant)
        inB = inBConstantDataHandle.asDouble()

        inSpiresDataHandle = data.inputValue(spLogSpiralCurve.inSpires)
        spires = inSpiresDataHandle.asDouble()

        deg = curve.degree
        nCVs = curve.numCVs

        curvePointArray = OpenMaya.MPointArray()
        curveKnotsArray = OpenMaya.MDoubleArray()

        for i in xrange(0, curve.numCVs):
            deg = (360 / nCVs) * i * spires
            theta = math.radians(deg)

            point = self.spiralCoordinate(theta, inA, inB)
            curvePointArray.append(point)

        knots = int(curve.numKnots)
        for i in xrange(0, knots):
            curveKnotsArray.append(i)

        outCurveData = OpenMaya.MFnNurbsCurveData()
        curveData = outCurveData.create()

        outCurve = OpenMaya.MFnNurbsCurve()
        outCurve.create(curvePointArray, curveKnotsArray, curve.degree, curve.form, 0, 0, curveData)

        outNurbsCurveDataHandle = data.outputValue(spLogSpiralCurve.outNurbsCurve)
        outNurbsCurveDataHandle.setMObject(curveData)
        
        data.setClean(plug)


def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.4", "Any")

    try:
        mplugin.registerNode(kNodeName, spLogSpiralCurve.id, spLogSpiralCurve.creator, spLogSpiralCurve.initialize)
    except:
        sys.stderr.write("Failed to register node %s\n" % kNodeName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterNode(spLogSpiralCurve.id)
    except:
        sys.stderr.write("Failed to deregister node %s\n" % kNodeName)
        raise
